abstract class Room {
    private int roomNumber;
    public double nightlyRate;
    private boolean booked;

    public Room(int roomNumber, double nightlyRate) {
        this.roomNumber = roomNumber;
        this.nightlyRate = nightlyRate;
        this.booked = false;
    }

    public void book() {
        if (!booked) {
            booked = true;
            System.out.println("Room " + roomNumber + " booked.");
        } else {
            System.out.println("Room " + roomNumber + " is already booked.");
        }
    }

    public void checkAvailability() {
        if (booked) {
            System.out.println("Room " + roomNumber + " is currently booked.");
        } else {
            System.out.println("Room " + roomNumber + " is available.");
        }
    }

    public abstract double calculateCharges(int numNights);
}
