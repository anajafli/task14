public class HotelBookingSystem {
    public static void main(String[] args) {
        StandardRoom standardRoom = new StandardRoom(10, 100.0);
        standardRoom.book();
        standardRoom.checkAvailability();

        DeluxeRoom deluxeRoom = new DeluxeRoom(201, 150.0, 2);
        deluxeRoom.book();
        deluxeRoom.checkAvailability();

        Suite suite = new Suite(301, 200.0, true);
        suite.book();
        suite.checkAvailability();
    }
}