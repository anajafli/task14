
class StandardRoom extends Room {
    public StandardRoom(int roomNumber, double nightlyRate) {
        super(roomNumber, nightlyRate);
    }

    public double calculateCharges(int numNights) {
        return nightlyRate * numNights;
    }
}
