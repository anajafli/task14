class DeluxeRoom extends Room {
    private int numBeds;

    public DeluxeRoom(int roomNumber, double nightlyRate, int numBeds) {
        super(roomNumber, nightlyRate);
        this.numBeds = numBeds;
    }

    public double calculateCharges(int numNights) {
        return nightlyRate * numNights;
    }
}