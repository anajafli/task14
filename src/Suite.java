class Suite extends Room {
    private boolean hasLivingRoom;

    public Suite(int roomNumber, double nightlyRate, boolean hasLivingRoom) {
        super(roomNumber, nightlyRate);
        this.hasLivingRoom = hasLivingRoom;
    }

    public double calculateCharges(int numNights) {
        return nightlyRate * numNights;
    }
}
